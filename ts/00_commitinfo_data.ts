/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/webdetector',
  version: '2.0.3',
  description: 'detect different environments within the browser'
}
