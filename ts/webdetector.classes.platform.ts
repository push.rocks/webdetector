export type TWebPlatform = 'android' | 'ios' | 'windows' | 'mac' | 'linux' | 'unknown';

export class Platform {
  public detectPlatform(): TWebPlatform {
    const userAgent = globalThis?.navigator?.userAgent?.toLowerCase();

    if (!userAgent) {
      return 'unknown';
    }

    if (/android/.test(userAgent)) {
      return 'android';
    } else if (/iphone|ipad|ipod/.test(userAgent)) {
      return 'ios';
    } else if (/win/.test(userAgent)) {
      return 'windows';
    } else if (/mac/.test(userAgent)) {
      return 'mac';
    } else if (/linux/.test(userAgent)) {
      return 'linux';
    }

    return 'unknown';
  }
}
